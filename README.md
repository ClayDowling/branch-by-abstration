# branch-by-abstration

A repository for teaching the [Branch By
Abstraction](https://martinfowler.com/bliki/BranchByAbstraction.html) pattern.

## The Goal

The goal of Branch by Abstraction is to introduce breaking changes in a way
that doesn't break the build, even if it is an intrusive change.  Using this
pattern allows you to introduce behavior changes without significant merge
conflicts, and avoids Boil the Ocean type refactors that cause contention on a
team.  Refactors using Branch by Abstraction can be committed and built in
small increments without breaking builds and minimal merge conflicts.

## The Exercise

This implementation of Conway's [Game of
Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) is intended for an
art installation.  Version 1 of this code shows the board as on or off cells.
In order to command a higher price for the final artwork, it's desirable to
visually indicate the age in generations of a living cell.

From a practical standpoint, this means that for any given cell on a board,
there should exist a mechanism to learn its age in generations (defined as one
call of the Game.Tick() method)

## Procedure

1. For this exercise **Fork This Repository**

1. Define the interface you want to your classes.

1. Test drive this new interface, maintaining existing behavior, without
   modifying any of the code which uses your classes.

1. Commit and push the change to your master branch.

1. Modify a consumer of your class to use the new interface, without changing
   the tests for the consumer.

1. Ensure that your tests for the consuming class pass.

1. Commit your changes and push to the master branch.

1. Repeat the last three steps for any other consumers of your class.

1. Remove the old interface to your class.

1. Ensure that all tests pass after you have removed the old interface.

1. Commit the changes and push to the master branch.

1. Test drive the new behavior that you want to see.

1. Commit the changes and push to the master branch.

## Notes

* This pattern emphasises many small changes, pushed to master.
* The pattern relies heavily on good test coverage to be safe.
* If your workflow relies on a code review process, it will quicly become
  cumbersome.  Adherents of this pattern often work directly from their master
  branch rather than working in a task branch.
* By emphasising the need for continuously working code, as well as tooling and
  team agreements that enforce the behavior, the risks that task branches are
  meant to mitigate are reduced or removed.
