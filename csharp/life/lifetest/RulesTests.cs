using FluentAssertions;
using life;
using Xunit;

namespace lifetest
{
    public class RulesTests
    {
        [Theory]
        [InlineData(3, true)]
        [InlineData(2, true)]
        [InlineData(3, false)]
        public void IsAlive_ReturnsTrue(int neighbors, bool currentlyAlive)
        {
            Rules.IsAlive(neighbors, currentlyAlive).Should().Be(true);
        }

        [Theory]
        [InlineData(0, true)]
        [InlineData(0, false)]
        [InlineData(1, true)]
        [InlineData(1, false)]
        [InlineData(2, false)]
        [InlineData(4, true)]
        [InlineData(4, false)]
        [InlineData(5, true)]
        [InlineData(6, false)]
        [InlineData(6, true)]
        [InlineData(6, false)]
        [InlineData(7, true)]
        [InlineData(7, false)]
        [InlineData(8, true)]
        [InlineData(8, false)]
        [InlineData(9, true)]
        [InlineData(9, false)]
        [InlineData(-1, true)]
        [InlineData(-1, false)]
        public void IsAlive_ReturnsFalse(int neighbors, bool currentlyAlive)
        {
            Rules.IsAlive(neighbors, currentlyAlive).Should().Be(false);
        }
    }
}