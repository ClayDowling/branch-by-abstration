using FluentAssertions;
using life;
using Xunit;

namespace lifetest
{
    public class BoardTests
    {
        public BoardTests()
        {
            for (int i = 0; i < Board.XSize; i++)
            {
                for (int j = 0; j < Board.YSize; j++)
                {
                    Board.Cell[Board.CurrentGeneration, i, j] = false;
                    Board.Cell[Board.FutureGeneration, i, j] = false;
                }
            }
        }
        
        [Fact]
        public void MakeFutureCurrentGeneration_GivenLifeFutureCellDeadCurrentCell_MakesCurrentCellLive()
        {
            Board.Cell[Board.CurrentGeneration, 1, 1] = false;
            Board.Cell[Board.FutureGeneration, 1, 1] = true;
            
            Board.MakeFutureCurrentGeneration();

            Board.Cell[Board.CurrentGeneration, 1, 1].Should().Be(true);
        }

        [Fact]
        public void Neighbors_withOneNeighbor_ReturnsOne()
        {
            setPosition(1, true, 5, 5);

            Board.Neighbors(5, 5).Should().Be(1);
        }

        [Fact]
        public void Neighbors_withTwoNeighbors_ReturnsTwo()
        {
            setPosition(2, true, 5, 5);
            setPosition(7, true, 5, 5);

            Board.Neighbors(5, 5).Should().Be(2);
        }

        [Fact]
        // Near, in this case, means that the neighbor is in an adjoining cell.
        // The board looks like the following, with 'X' representing the square we
        // are interested in, and the 'o' cells being other living cells.
        //
        //   34567
        // 3 o
        // 4
        // 5   X
        // 6    o
        // 7
        public void Neighbors_WithOneNearNeighborAndOneFar_ReturnsOne()
        {
            setPosition(5, true, 3, 3);
            setPosition(9, true, 5, 5);

            Board.Neighbors(5, 5).Should().Be(1);
        }

        [Fact]
        public void Neighbors_WithCurrentCellLiveAndNoNeighbors_ReturnsZero()
        {
            setPosition(5, true, 6, 6);

            Board.Neighbors(6, 6).Should().Be(0);
        }

        [Fact]
        public void Neighbors_ForPositionsBelowZero_ReturnsZero()
        {
            setPosition(1, true, -1, -1);

            Board.Neighbors(-1, -1).Should().Be(0);
        }

        [Fact]
        public void Neighbors_ForPositionAboveXSizeOrYSize_ReturnsZero()
        {
            setPosition(9, true, Board.XSize, Board.YSize);

            Board.Neighbors(Board.XSize, Board.YSize).Should().Be(0);
        }
        
        private class Point
        {
            public int X;
            public int Y;

            public Point(int x, int y)
            {
                X = x;
                Y = y;
            }
        }

        private readonly Point[] _phonepad = new Point[]
        {
            new Point(-10, -10), // Never used
            new Point(-1, -1),
            new Point(0, -1),
            new Point(+1, -1),
            new Point(-1, 0),
            new Point(0, 0),
            new Point(+1, 0),
            new Point(-1, +1),
            new Point(0, +1),
            new Point(+1, +1),
        };

        // setPosition sets state around the point of interest defined by x, y.
        // The phonePosition parameter describes the relationship to the point of
        // interest in terms of the numbers on a touch-tone telephone pad.  For those
        // born after 1990, the keys on a touch-tone phone were arranged as follows:
        //
        // 1 2 3
        // 4 5 6
        // 7 8 9
        //
        // In this scheme, 5 represents the point of interest.
        private void setPosition(int phonePosition, bool state, int x, int y)
        {
            Point position = _phonepad[phonePosition];
            var xprime = x + position.X;
            var yprime = y + position.Y;

            if (xprime >= 0 && xprime < Board.XSize && yprime >= 0 && yprime < Board.YSize)
            {
                Board.Cell[Board.CurrentGeneration, xprime, yprime] = state;
            }
        }
        
    }
}