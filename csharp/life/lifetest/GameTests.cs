using FluentAssertions;
using life;
using Xunit;

namespace lifetest
{
    public class GameTests
    {

        public GameTests()
        {
            for (int i = 0; i < Board.XSize; i++)
            {
                for (int j = 0; j < Board.YSize; j++)
                {
                    Board.Cell[Board.CurrentGeneration, i, j] = false;
                }
            }
        }
        
        [Fact]
        public void Tick_GivenVerticalBar_LeavesHorizontalBar()
        {
            Board.Cell[Board.CurrentGeneration, 5, 4] = true;
            Board.Cell[Board.CurrentGeneration, 5, 5] = true;
            Board.Cell[Board.CurrentGeneration, 5, 6] = true;
            
            Game.Tick();

            Board.Cell[Board.CurrentGeneration, 5, 4].Should().Be(false);
            Board.Cell[Board.CurrentGeneration, 5, 5].Should().Be(true);
            Board.Cell[Board.CurrentGeneration, 5, 6].Should().Be(false);
            Board.Cell[Board.CurrentGeneration, 4, 5].Should().Be(true);
            Board.Cell[Board.CurrentGeneration, 6, 5].Should().Be(true);
        }
    }
}