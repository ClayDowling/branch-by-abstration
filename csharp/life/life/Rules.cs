namespace life
{
    public static class Rules
    {
        public static bool IsAlive(int neighbors, bool isAlive)
        {
            switch (neighbors)
            {
                case 2:
                    return isAlive;
                case 3:
                    return true;
                default:
                    return false;
            }            
        }
    }
}