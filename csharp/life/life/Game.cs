namespace life
{
    public static class Game
    {
        public static void Tick()
        {
            for (int i = 0; i < Board.XSize; i++)
            {
                for (int j = 0; j < Board.YSize; j++)
                {
                    Board.Cell[Board.FutureGeneration, i, j] = Rules.IsAlive(Board.Neighbors(i, j),
                        Board.Cell[Board.CurrentGeneration, i, j]);
                }
            }
            Board.MakeFutureCurrentGeneration();
        }
    }
}