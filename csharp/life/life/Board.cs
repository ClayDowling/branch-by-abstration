namespace life
{
    public static class Board
    {
        public static int XSize = 32;
        public static int YSize = 32;
        public static int CurrentGeneration = 0;
        public static int FutureGeneration = 1;

        public static bool[,,] Cell = new bool[2, XSize, YSize];

        public static void MakeFutureCurrentGeneration()
        {
            if (0 == CurrentGeneration)
            {
                CurrentGeneration = 1;
                FutureGeneration = 0;
            }
            else
            {
                CurrentGeneration = 0;
                FutureGeneration = 1;
            }
        }

        public static int Neighbors(int x, int y)
        {
            int sum = 0;
            for (int i = x - 1; i <= x + 1; i++)
            {
                for (int j = y - 1; j <= y + 1; j++)
                {
                    if ( i >= 0 && i < XSize && j >= 0 && j < YSize && (i != x || j != y) )
                    {
                        sum += Cell[CurrentGeneration, i, j] ? 1 : 0;
                    }
                }
            }

            return sum;
        }
    }
}