#include "board.h"

#include <check.h>
#include <stdio.h>

void setup() {
  memset(cell, 0, sizeof(cell));
  current_generation = 0;
  future_generation = 1;
}

void teardown() {
  // Do Nothing
}

START_TEST(getCurrentNeighbors_deadCellWithOneNeighbor_returnsOne) {
  cell[future_generation][1][1] = false;
  cell[future_generation][0][0] = true;
  board_makeFutureStateCurrent();
  ck_assert_int_eq(1, board_currentNeighbors(1, 1));
}
END_TEST

START_TEST(getCurrentNeighbors_liveCellSurroundedOnAllSides_returnsEight) {
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      cell[future_generation][i][j] = true;
    }
  }
  board_makeFutureStateCurrent();

  ck_assert_int_eq(8, board_currentNeighbors(1, 1));
}
END_TEST

TCase *tcase_board() {
  TCase *tc = tcase_create("board");

  tcase_add_checked_fixture(tc, setup, teardown);

  tcase_add_test(tc, getCurrentNeighbors_deadCellWithOneNeighbor_returnsOne);
  tcase_add_test(tc,
                 getCurrentNeighbors_liveCellSurroundedOnAllSides_returnsEight);
  return tc;
}

Suite *suite_board() {
  Suite *s = suite_create("board");
  suite_add_tcase(s, tcase_board());
  return s;
}