#ifndef _SUITES_H_
#define _SUITES_H_

#include <check.h>

Suite *suite_rules();
Suite *suite_board();
Suite *suite_game();

#endif