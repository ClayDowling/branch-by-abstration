#include "board.h"

int current_generation = 0;
int future_generation = 1;
static bool cells_initialized = false;
bool cell[2][BOARD_X][BOARD_Y];

void initialize_cells() {
  for (int x = 0; x < BOARD_X; ++x) {
    for (int y = 0; y < BOARD_Y; ++y) {
      cell[current_generation][x][y] = false;
      cell[future_generation][x][y] = false;
    }
  }
  cells_initialized = true;
}

void board_makeFutureStateCurrent() {
  if (0 == current_generation) {
    current_generation = 1;
    future_generation = 0;
  } else {
    current_generation = 0;
    future_generation = 1;
  }
}

int board_currentNeighbors(unsigned int x, unsigned int y) {
  int count = 0;
  for (int i = x - 1; i <= x + 1; ++i) {
    for (int j = y - 1; j <= y + 1; ++j) {
      if (x != i || y != j) {
        count += cell[current_generation][i][j] ? 1 : 0;
      }
    }
  }

  return count;
}