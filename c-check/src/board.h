#ifndef _BOARD_H_
#define _BOARD_H_

#include <stdbool.h>

#define BOARD_X 32
#define BOARD_Y 64

extern bool cell[2][BOARD_X][BOARD_Y];

extern int current_generation;
extern int future_generation;

void board_makeFutureStateCurrent();
int board_currentNeighbors(unsigned int x, unsigned int y);

#endif